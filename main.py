from _pydecimal import InvalidOperation, Decimal

import pandas as pd
import matplotlib.pyplot as plt
import pandas_schema
import xlrd
import datetime

from pandas_schema import Column
from pandas_schema.validation import CustomElementValidation


def check_decimal(dec):
    try:
        Decimal(dec)
    except InvalidOperation:
        return False
    return True


def check_int(num):
    try:
        int(num)
    except ValueError:
        return False
    return True


def check_title(str):
    if len(str) > 3:
        return True
    else:
        return False


def date_validity(data):
    try:
        temp = datetime.date.fromisoformat(data)
        now = datetime.datetime.now()
        return now > temp
    except:
        return False


def check_price(data):
    return data > 0

def check_range(data):
    try:
        if(data < 1000):
            return True
        else:
            return False
    finally:
        return False

#global rule

decimal_validation = [CustomElementValidation(lambda d: check_decimal(d), 'is not decimal')]
int_validation = [CustomElementValidation(lambda i: check_int(i), 'is not integer')]
null_validation = [CustomElementValidation(lambda d: d is not pd.np.nan, 'this field cannot be null')]
date_validation = [CustomElementValidation(lambda d: date_validity(d), 'Date Time is invalid')]
title_validation = [CustomElementValidation(lambda d: check_title(d), 'title is invalid')]
price_validation = [CustomElementValidation(lambda d: check_price(d), 'price is invalid')]
range_validation = [CustomElementValidation(lambda d: check_range(d), 'range is invalid')]


product_schema = pandas_schema.Schema([
    Column('ID', date_validation + null_validation),
    Column('product_title_fa', title_validation + null_validation),
    Column('product_title_en', title_validation + null_validation),
    Column('category_title_fa', title_validation + null_validation),
    Column('category_title_en', title_validation + null_validation),
    Column('product_attributes', null_validation),
    Column('category_keywords',),
    Column('url_code', ),
])

order_schema = pandas_schema.Schema([
    Column('ID_Order', ),
    Column('ID_Customer', null_validation + int_validation),
    Column('ID_Item', null_validation + int_validation),
    Column('DateTime_CartFinalize', null_validation + date_validation),
    Column('Amount_Gross_Order', null_validation + int_validation),
    Column('city_name_fa', null_validation + title_validation),
    Column('Quantity_item', null_validation + int_validation),
])

comment_schema = pandas_schema.Schema([
    Column('comment', null_validation + title_validation),
    Column('product_id', null_validation + int_validation),
    Column('confirmed_at', null_validation + date_validation),
])

quality_schema = pandas_schema.Schema([
    Column('product_id', null_validation),
    Column('product_title', null_validation + title_validation),
    Column('title_en', null_validation + title_validation),
    Column('user_id', null_validation + int_validation),
    Column('likes', null_validation + int_validation),
    Column('dislikes', null_validation + int_validation),
    Column('verification_status', null_validation + title_validation),
    Column('recommend', null_validation + title_validation),
    Column('title', null_validation + title_validation),
    Column('comment', null_validation + title_validation),
    Column('advantages', null_validation + title_validation),
    Column('disadvantages', null_validation + title_validation),
])
history_schema = pandas_schema.Schema([
    Column('ID', null_validation + title_validation),
    Column('product_variant_id', null_validation + title_validation),
    Column('selling_price', null_validation + int_validation),
    Column('rrp_price', null_validation + int_validation),
    Column('base_price', null_validation + int_validation),
    Column('buy_price', null_validation + int_validation),
    Column('order_limit', null_validation + int_validation),
    Column('start_at', null_validation + date_validation),
    Column('end_at', null_validation + date_validation),
    Column('tags', null_validation + title_validation),
    Column('show_in_price_history', null_validation + title_validation),
])






schema = pandas_schema.Schema([
    Column('start_at', date_validation + null_validation),
    Column('end_at', date_validation + null_validation),
])

# readTables
comments = pd.read_excel(r'dataset\comment.xlsx')
qualities = pd.read_excel(r'dataset\keifiat.xlsx')
products = pd.read_excel(r'dataset\product.xlsx')
orderHistory = pd.read_csv(r'dataset/tarikhche kharid.csv')
orders = pd.read_csv(r'dataset/orders.csv')

# temp = orderHistory[['start_at', 'end_at']]
# print(temp)
# errors = product_schema.validate(products)



def formoule_one(item):
    return len(item.dropna()) / len(item) * 100

# print("comment : ")
# print(formoule_one(comments[['comment', 'product_id']]))
# print("keifiat : ")
# print(formoule_one(qualities[['product_title', 'title_en', 'likes', 'dislikes', 'recommend', 'title', 'comment']]))
# print("product : ")
# print(
#     formoule_one(products[['product_title_fa', 'product_title_en', 'category_title_fa', 'product_attributes', 'category_keywords', 'url_code']])
#     )
#


Q1 = orderHistory.quantile(0.25)
Q3 = orderHistory.quantile(0.75)
IQR = Q3 - Q1
# print(df < (Q1 - 1.5 * IQR)) |(df > (Q3 + 1.5 * IQR))
res = (orderHistory < (Q1 - 1.5 * IQR)) | ((orderHistory > (Q3 + 1.5 * IQR)))

print(len(res == False))

